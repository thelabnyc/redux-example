import * as React from "react";
import { Component } from "react";
import { connect } from "react-redux";
import { IState, getBar } from "../reducer";
import { setBar } from "./foo.actions";
import { FooComponent } from "./foo.component";


interface IFooProps {
    bar: string;
    setBar: (bar: string) => void;
    requestBar: () => void;
}

class FooContainer extends Component<IFooProps, {}> {
    componentDidMount() {
        this.props.requestBar();
    }

    greatBusinessLogic = () => {
        this.props.setBar("something great");
    }
    render() {
        return (
            <FooComponent
                bar={this.props.bar}
                doSomething={this.greatBusinessLogic}
            />
        )
    }
}

export default connect((state: IState) => ({
    bar: getBar(state),
}),
(dispatch: Function) => ({
    setBar: (bar: string) => dispatch(setBar(bar)),
    requestBar: () => dispatch({type: 'REQUEST_BAR'}),
})
)(FooContainer);