import { call, put, takeEvery, takeLatest } from 'redux-saga/effects'

function* fetchFakeApi() {
    let apiData = "pretend this is from a server";
    yield put({type: "Foo/SET_BAR", payload: apiData});
}

export function* mySaga() {
    yield takeEvery("REQUEST_BAR", fetchFakeApi);
}