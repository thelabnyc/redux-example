import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import FooContainer from "./foo/foo.container";
import { store } from "./store";

ReactDOM.render(
  <Provider store={store}>
    <FooContainer />
  </Provider>,
  document.getElementById('app')
);